﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DynamicwebProject
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
           //AreaRegistration.RegisterAllAreas();
           //RouteConfig.RegisterRoutes(RouteTable.Routes);

			Dynamicweb.Frontend.GlobalAsaxHandler.Application_Start(sender, e);
        }

		void Session_Start(object sender, EventArgs e)
		{
			Dynamicweb.Frontend.GlobalAsaxHandler.Session_Start(sender, e);
		}

		void Application_BeginRequest(object sender, EventArgs e)
		{
			Dynamicweb.Frontend.GlobalAsaxHandler.Application_BeginRequest(sender, e);
		}

		void Application_AuthenticateRequest(object sender, EventArgs e)
		{
			Dynamicweb.Frontend.GlobalAsaxHandler.Application_AuthenticateRequest(sender, e);
		}

		void Application_OnPreRequestHandlerExecute(object sender, EventArgs e)
		{
			Dynamicweb.Frontend.GlobalAsaxHandler.Application_OnPreRequestHandlerExecute(sender, e);
		}

		void Application_Error(object sender, EventArgs e)
		{
			Dynamicweb.Frontend.GlobalAsaxHandler.Application_Error(sender, e);
		}

		void Session_End(object sender, EventArgs e)
		{
			Dynamicweb.Frontend.GlobalAsaxHandler.Session_End(sender, e);
		}

		void Application_End(object sender, EventArgs e)
		{
			Dynamicweb.Frontend.GlobalAsaxHandler.Application_End(sender, e);
		}

		void Application_EndRequest(object sender, EventArgs e)
		{
			Dynamicweb.Frontend.GlobalAsaxHandler.Application_EndRequest(sender, e);
		}
	}
}