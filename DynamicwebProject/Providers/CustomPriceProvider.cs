﻿
using Dynamicweb.Ecommerce.Prices;
using Dynamicweb.Ecommerce.Products;
using Dynamicweb.Ecommerce.International;
using Dynamicweb.Security.UserManagement;

namespace T3
{
    public class CustomPriceProvider : PriceProvider
    {
        public override PriceRaw FindPrice(Product product, double quantity, string variantID, Currency currency, string unitID, User user)
        {
            // Get the price from the DefaultPriceProvider
            DefaultPriceProvider defaultProvider = new DefaultPriceProvider();
            PriceRaw rawPrice = defaultProvider.FindPrice(product, quantity, variantID, currency, unitID, user);

            if (product.Name.StartsWith("M"))
            {
                rawPrice.Price = rawPrice.Price / 2;
                return rawPrice;
            }
            return null;
        }
    }
}
