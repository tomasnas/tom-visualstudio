﻿var Expand = function () { }

document.addEventListener("DOMContentLoaded", function (event) {
    Expand.initExpandTriggers();

    //Make it work with Ajax loaded content
    var ajaxContainer = document.getElementsByClassName("js-handlebars-root");
    if (ajaxContainer.length > 0) {
        for (var i = 0; i < ajaxContainer.length; i++) {
            ajaxContainer[i].addEventListener('contentLoaded', function (e) {
                Expand.initExpandTriggers();
            }, false);
        }
    }
});

Expand.prototype.initExpandTriggers = function () {
    document.querySelectorAll("[data-expand]").forEach(function (trigger) {
        var change = function () {
            var expandBlocks = document.querySelectorAll("[data-trigger=" + trigger.getAttribute("data-expand") + "]");
            expandBlocks.forEach(function (block) {
                if (block.classList.contains("js-expand-hide")) {
                    Expand.toggleClass(block, "expandable--collapsed", trigger.checked);
                } else {
                    Expand.toggleClass(block, "expandable--collapsed", !trigger.checked);
                }
            });
        };
        trigger.addEventListener('change', change);
        change(); //sync with start values
    });
}

Expand.prototype.toggleClass = function (element, className, addClass) {
    if (addClass) {
        element.classList.add(className);
    } else {
        element.classList.remove(className);
    }
}

var Expand = new Expand();