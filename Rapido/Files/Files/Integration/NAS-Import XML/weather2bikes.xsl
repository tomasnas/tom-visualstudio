<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output cdata-section-elements="column"/>
    <xsl:template match="/">
		<xsl:text>&#xa;</xsl:text>
        <tables>
		<xsl:text>&#xa;&#x20;&#x20;</xsl:text>
            <table tableName="EcomProducts">
			<xsl:text>&#xa;&#x20;&#x20;&#x20;&#x20;</xsl:text>
				<item table="EcomProducts">
					<xsl:for-each select="tables/table/item">
						<xsl:for-each select="column">
							<xsl:choose>
								<xsl:when test='@columnName="id"'>
									<xsl:text>&#xa;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;</xsl:text>
									<column columnName="ProductId">
										<xsl:value-of select="."/>
									</column>
								</xsl:when>
								<xsl:when test='@columnName="Ciudad"'>
									<xsl:text>&#xa;&#x20;&#x20;&#x20;&#x20;&#x20;&#x20;</xsl:text>
									<column columnName="ProductName">
										<xsl:value-of select="."/>
									</column>
								</xsl:when>
							</xsl:choose>
						</xsl:for-each>
					</xsl:for-each>
				<xsl:text>&#xa;&#x20;&#x20;&#x20;&#x20;</xsl:text>
				</item>
			<xsl:text>&#xa;&#x20;&#x20;</xsl:text>
			</table>
		<xsl:text>&#xa;</xsl:text>
        </tables>
    </xsl:template>
</xsl:stylesheet>
